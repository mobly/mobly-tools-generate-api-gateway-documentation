const S3 = require('aws-sdk/clients/s3')
const fs = require('fs')

const bucket = new S3({
    apiVersion: '2006-03-01',
    accessKeyId: process.env.AWS_ACCESS_KEY_ID,
    secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY
})

const upload = async (stage, name) => {
    
    try {
        const body = fs.readFileSync('./index.html', 'utf-8')
        const up = bucket.putObject({
            ACL: 'public-read',
            Body: body,
            Bucket: `mobly.${stage}.services.documentation`,
            Key: `${name}/index.html`,
            ContentEncoding: 'utf-8',
            ContentType: 'text/html',
            Metadata:{
                'Content-Type': 'text/html'
            }
        }).promise()
        return up
    } catch (e) {
        throw e
    }
}

const makeWebsiteConfiguration = async (stage) => {
    try {
        return bucket.putBucketWebsite({
            Bucket: `mobly.${stage}.services.documentation`,
            WebsiteConfiguration: {
                ErrorDocument: {
                    Key: `error.html`
                   }, 
                   IndexDocument: {
                    Suffix: `index.html`
                   }
            }
        }).promise()
    } catch (e) {
        throw e
    }
}

module.exports = {upload, makeWebsiteConfiguration}