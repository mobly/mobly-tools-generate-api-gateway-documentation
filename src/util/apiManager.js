let APIGATEWAY = require('aws-sdk/clients/apigateway')
const {argv} = require('./arguments')

let apig = new APIGATEWAY({
    apiVersion: '2015-07-09',
    region: argv['api-region'],
    accessKeyId: process.env.AWS_ACCESS_KEY_ID,
    secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY
})

const getApiId = async (apiName, stage) => {
    try {
        const apis = await apig.getRestApis().promise()

        const compositeName = `${stage}-${apiName}`
        if (apis.items.length == 0) throw ('There are no APIs in this account')

        const match = apis.items.find(api => api.name === compositeName)

        if (!match) throw (`Couldn\'t find api: ${apiName}`)

        return match.id
    } catch (e) {
        console.log("EXCEPTION: " + e)
        throw new Error(e)
    }
}

const getDocumentation = async (id, stage) => {
    try {
        const api = await apig.getExport({
            restApiId: id,
            stageName: stage,
            exportType: 'swagger'
        }).promise()
        return api.body;
    } catch (e) {
        console.log("Exception :: " + e)
        throw new Error(e)
    }
}

module.exports = {getApiId, getDocumentation}