const argv = require('yargs')
    .command('aws-apigateway-get-documentation', 'Get the AWS API Gateway documentation by the name of the resource and upload it to a bucket.')
    .options({
        'api-name': {
            alias: 'an',
            describe: 'The AWS API Gateway Resource Name',
            required: true
        },
        'api-region': {
            alias: 'ar',
            describe: 'The region of the Resouce above',
            default: 'us-east-1'
        },
        'api-stage': {
            alias: 'as',
            describe: 'The stage to look for documentation.',
            default: 'dev'
        },
    })
    .help()
    .argv

module.exports = {argv}