#!/usr/bin/env node

const {argv} = require('../util/arguments')
const {getApiId, getDocumentation} = require('../util/apiManager')
const {upload, makeWebsiteConfiguration} = require('../util/bucketManager')

console.log('#getApiId')
console.log(getApiId)

const fs = require('fs')
const { promisify } = require('util')
const exec = promisify(require('child_process').exec)

module.exports = async () => {

    if (!(process.env.AWS_ACCESS_KEY_ID || process.env.AWS_SECRET_ACCESS_KEY)) {
        throw new Error('You must have both AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY defined in your environment.\n')
    }
    
    console.log(
    `
                ~USING~:
    [ApiGateway Resource Name]: ${argv['api-name']}
    [ApiGateway Region]: ${argv['api-region']}
    [ApiGateway Stage]: ${argv['api-stage']}
    `
    )

    const restId = await getApiId(argv['api-name'], argv['api-stage'], argv['api-region'])

    const docJson = await getDocumentation(restId, argv['api-stage'])

    const parsed = JSON.parse(docJson)

    try {
        for (let path in parsed.paths) {
            if (parsed.paths[path].options) delete parsed.paths[path].options
        }
        console.log(`Writing 'documentation.json' ...`)
        fs.writeFileSync('documentation.json', JSON.stringify(parsed))
        console.log('Write success')
        console.log('Generating index.html...')
        const { stdout, stderr, error } = await exec(`npx redoc-cli bundle -o index.html ./documentation.json`)
        if (error) {
            console.error(error)
            return 1
        }

        console.log('Uploading index.html to S3 Bucket')

        await upload(argv['api-stage'], argv['api-name'])

        console.log('Upload Successful ...\nConfiguring bucket for website ...')

        await makeWebsiteConfiguration(argv['api-stage'])

        console.log('Documentation uploaded to:')
        console.log(`https://mobly.${argv['api-stage']}.services.documentation.s3.amazonaws.com/${argv['api-name']}/index.html`)

        return 0

    } catch (e) {
        console.log(`
        Error writing documentation file.
        Check the output :
        `)
        console.error(e)
        return 1
    }
}