#!/usr/bin/env node
const gen = require('./core/generateDocumentation')
// process deixa a saída natural do node ao invés de forçar a saída do processo
// setar exitCode é boa prática ao invés de process.exit()
// process.exit() deve ser chamado em casos específicos caso seja estritamente necessário
// forçar o encerramento do processo.

gen().then(result => {
  process.exitCode = result
})
