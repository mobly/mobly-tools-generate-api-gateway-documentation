# [2.0.0](https://bitbucket.org/mobly/mobly-tools-generate-api-gateway-documentation/compare/v1.0.2...v2.0.0) (2019-12-27)


### chore

* **options:** Refactory ([83c5143](https://bitbucket.org/mobly/mobly-tools-generate-api-gateway-documentation/commits/83c51433edef70a455ed5068a103b927908aa6c6))


### BREAKING CHANGES

* **options:** Full Refactory of this package
Read the readme.md for more changes info

## [1.0.2](https://bitbucket.org/mobly/mobly-tools-generate-api-gateway-documentation/compare/v1.0.1...v1.0.2) (2019-11-04)


### Bug Fixes

* **pipeline:** Mudando mode de utilização do pacote. ([cb057e8](https://bitbucket.org/mobly/mobly-tools-generate-api-gateway-documentation/commits/cb057e8))

## [1.0.1](https://bitbucket.org/mobly/mobly-tools-generate-api-gateway-documentation/compare/v1.0.0...v1.0.1) (2019-11-04)


### Bug Fixes

* **readme:** Corrigindo o método de utilização do pacote. ([92682eb](https://bitbucket.org/mobly/mobly-tools-generate-api-gateway-documentation/commits/92682eb))

# 1.0.0 (2019-10-30)


### chore

* **pipeline:** Fixing pipeline and adding npm push ([87d57df](https://bitbucket.org/mobly/mobly-tools-generate-api-gateway-documentation/commits/87d57df))
* **semantic-release:** Adding files to semantic release ([65cc818](https://bitbucket.org/mobly/mobly-tools-generate-api-gateway-documentation/commits/65cc818))


### BREAKING CHANGES

* **pipeline:** First deploy
* **semantic-release:** Adding version bump
