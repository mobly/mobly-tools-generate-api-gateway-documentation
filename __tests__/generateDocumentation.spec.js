jest.mock('../src/util/arguments', () => {
    return {
      command: function () {
        return this
      },
      options: function () {
        return this
      },
      help: function () {
        return this
      },
      argv: {
        'api-name': 'name',
        'api-stage': 'stage',
        'api-region': 'region'
      }
    }
  })

describe('[GENERATE]', () => {
    beforeEach(() => jest.resetModules())

    it('Should succeed in archive writing', async done => {

        jest.mock('../src/util/apiManager', () => {
          return {
            getApiId: () => Promise.resolve(),
            getDocumentation: () => Promise.resolve(JSON.stringify({}))
          }
        })

        jest.mock('../src/util/bucketManager', () => {
            return {
                upload: () => Promise.resolve(),
                makeWebsiteConfiguration: () => Promise.resolve()
            }
        })

        jest.mock('util', () => {
            return {
                promisify: (x) => x
            }
        })

        jest.mock('fs', () => {
            return {
                writeFileSync: () => true
            }
        })

        jest.mock('child_process', () => {
            return {
              exec: () => ({
                stdin: '',
                stderr: '',
                error: null
              })
            }
        })
        const gen = require('../src/core/generateDocumentation')
        const res = await gen()
        expect(res).toBe(0)
        done()
    })
})