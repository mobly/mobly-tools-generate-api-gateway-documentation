jest.mock('../src/util/arguments', () => {
  return {
    command: function () {
      return this
    },
    options: function () {
      return this
    },
    help: function () {
      return this
    },
    argv: {
      'api-name': 'name',
      'api-stage': 'stage',
      'api-region': 'region'
    }
  }
})
describe('[APIMANAGER]', () => {
  beforeEach(() => {
    jest.resetModules()
  })
  it('Should find the ID of the API', async done => {
    jest.mock('aws-sdk/clients/apigateway', () => {
      return function () {
        return {
          getRestApis: function () {
            return this
          },
          promise: function () {
            return Promise.resolve({
              items: [
                {
                  id: 'z77ef8zvve',
                  name: 'dev-customer-service',
                  createdDate: '2019-11-14T19:09:30.000Z',
                  apiKeySource: 'HEADER',
                  endpointConfiguration: '[Object]',
                  tags: 'WE FOUND'
                }
              ]
            })
          }
        }
      }
    })
    const apiManager = require('../src/util/apiManager')
    const id = await apiManager.getApiId('customer-service', 'dev')
    expect(id).toBe('z77ef8zvve')
    done()
  })

  it('Should not find API ID', async done => {
    jest.mock('aws-sdk/clients/apigateway', () => {
      return function () {
        return {
          getRestApis: function () {
            return this
          },
          promise: function () {
            return Promise.resolve({
              items: [
                {
                  id: 'z77ef8zvve',
                  name: 'dev-customer-service',
                  createdDate: '2019-11-14T19:09:30.000Z',
                  apiKeySource: 'HEADER',
                  endpointConfiguration: '[Object]',
                  tags: 'NOT FIND API'
                }
              ]
            })
          }
        }
      }
    })
    const apiManager = require('../src/util/apiManager')
    try {
      await apiManager.getApiId('no-name', 'not-found')
    } catch (e) {
      expect(e.message).toBe(`Couldn't find api: no-name`)
      done()
    }
  })

  it('Should have no APIs', async done => {
    jest.mock('aws-sdk/clients/apigateway', () => {
      return function () {
        return {
          getRestApis: function () {
            return this
          },
          promise: function () {
            return Promise.resolve({ items: [] })
          }
        }
      }
    })
    const apiManager = require('../src/util/apiManager')
    try {
      await apiManager.getApiId('no-name', 'not-found')
    } catch (e) {
      expect(e.message).toBe(`There are no APIs in this account`)
      done()
    }
  })

  it('Should receive body correctly', async done => {
    jest.mock('aws-sdk/clients/apigateway', () => {
      return function () {
        return {
          getExport: function () {
            return this
          },
          promise: function () {
            return Promise.resolve({ body: {} })
          }
        }
      }
    })
    const apiManager = require('../src/util/apiManager')

    const api = await apiManager.getDocumentation('id', 'stage')
    expect(typeof api).toBe('object')
    done()
  })
  it('Should receive body correctly', async done => {
    jest.mock('aws-sdk/clients/apigateway', () => {
      return function () {
        return {
          getExport: function () {
            return this
          },
          promise: function () {
            throw 'Error'
          }
        }
      }
    })
    const apiManager = require('../src/util/apiManager')
    try{
await apiManager.getDocumentation('id', 'stage')
    } catch (e) {
      expect(e.message).toBe('Error')
    }
    done()
  })
})
