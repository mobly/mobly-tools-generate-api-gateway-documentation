describe('[BUCKETMANAGER]', () => {
    beforeEach(() => {
        jest.resetModules()
    })

    it('Should have "upload" as an instance of Promise', async done => {
        jest.mock('fs', () => ({
            readFileSync: () => true
        }))
        jest.mock('aws-sdk/clients/s3', () => {
            return function () {
                return {
                    putObject: function () {
                        return this
                    },
                    promise: function () {
                        return new Promise()
                    }
                }
            }
        })

        const bucketManager = require('../src/util/bucketManager')
        const upload = bucketManager.upload('dev', 'test')

        expect(upload instanceof Promise).toBe(true)
        done()
    })
    it('Should throw an error trying to settle a promise', async done => {
        jest.mock('fs', () => ({
            readFileSync: () => true
        }))
        jest.mock('aws-sdk/clients/s3', () => {
            return function () {
                return {
                    putObject: function () {
                        return this
                    },
                    promise: function () {
                        throw 'error'
                    }
                }
            }
        })
        const bucketManager = require('../src/util/bucketManager')
        try {
            await bucketManager.upload('dev', 'test')
        } catch (e) {
            console.log(e)
            expect(e).toBe('error')
            done()
        }
    })

    it('Should have "makeWebsiteConfiguration" as an instance of Promise', async done => {
        jest.mock('fs', () => ({
            readFileSync: () => true
        }))
        jest.mock('aws-sdk/clients/s3', () => {
            return function () {
                return {
                    putBucketWebsite: function () {
                        return this
                    },
                    promise: function () {
                        return new Promise()
                    }
                }
            }
        })
        const bucketManager = require('../src/util/bucketManager')
        const web = bucketManager.makeWebsiteConfiguration('dev')
        expect(web instanceof Promise).toBe(true)
        done()
    })

    it('Should have throw an exception', async done => {
        jest.mock('fs', () => ({
            readFileSync: () => true
        }))
        jest.mock('aws-sdk/clients/s3', () => {
            return function () {
                return {
                    putBucketWebsite: function () {
                        return this
                    },
                    promise: function () {
                        throw 'error'
                    }
                }
            }
        })
        const bucketManager = require('../src/util/bucketManager')
        try{
        bucketManager.makeWebsiteConfiguration('dev')
        }catch (e){
            expect(e).toBe('error')
        }
        done()
    })
})