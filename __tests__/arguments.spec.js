describe('[ARGV]', () => {
  it('Should have been called', (done) => {
    const {argv} = require('../src/util/arguments')
    jest.mock('yargs', () => {
      return {
        command: function(){
          return this
        },
        options: function() {
          return this
        },
        help: function() {
          return this
        },
        argv: {
          'api-name': 'name',
          'api-stage': 'stage',
          'api-region': 'region'
        }
      }
    })
    expect(argv).toHaveProperty('api-name', 'name')
    done()
  })
})
