# @mobly/tools/generate-api-gateway-documentation


## Instruções de Uso

Para usar este repositório em seu CI/CD, execute os comando abaixo (nos comandos do pipeline):  

```
npm i -g @moblybr/tools-generate-api-gateway-documentation

aws-apigateway-get-documentation --an api-name --as stage --ar region
```

> É importante ressaltar que você deve ter as variáveis  
    - $AWS_ACCESS_KEY_ID  
    - $AWS_SECRET_ACCESS_KEY  
setadas no repositório ou especificar na hora de executar o script

### Setando as variáveis no Bitbucket

Vá nas _Settings_, seção de _Pipelines_ do seu repositório:

1. Clique em `Deployments`
2. Clique no Stage desejado
3. Clique em `Env Variables`
4. Adicione cada uma delas com o valor desejado.

> Atente-se que se você deixar a variável como _Secured_, você não poderá executar `echo $VARIAVEL` dentro do pipeline.

Todos os `swaggers` serão colocados no mesmo bucket: `mobly.<stage>.services.documentation/<service-name>` e a URL para conferir se a página está disponível é:  
`
http://mobly.<stage>.services.documentation.s3-website-<region>.amazonaws.com/<service-name>/index.html
`  

### Exemplo de bitbucket-pipeline.yml

```
image: node:8.10-stretch

options:
  size: 2x

pipelines:
  default:
    - step:
        name: Test and coverage
        caches:
          - node
        script:
          - npm install
          - npm test

  branches:

    develop:
      - step:
          name: Test and coverage
          caches:
            - node
          script:
            - npm install
            - npm test

      - step:
          deployment: test
          name: Deploy to dev
          caches:
            - node
          script:
            - npm install
            - npm run config:credentials:dev
            - npm run deploy:dev
      
      - step:
          name: Generate documentation
          deployment: test
          caches:
            - node
          script:
            - npm i -g @moblybr/tools-generate-api-gateway-documentation
            - aws-apigateway-get-documentation --an api-name --as stage --ar region

      - step:
          deployment: staging
          name: Deploy to staging
          trigger: manual
          caches:
            - node
          script:
            - npm install
            - npm run config:credentials:staging
            - npm run deploy:staging
```
