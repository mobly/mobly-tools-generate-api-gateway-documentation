stage=$1
region=$2
outputFileName="documentation.json"
APPLICATION_NAME="$1-$3"
restid=`aws apigateway get-rest-apis --output=json --region=$region | APPLICATION_NAME=$APPLICATION_NAME node util/extract-api.js`

echo "RestID :: $restid"

aws apigateway get-export \
  --rest-api-id=$restid \
  --stage-name=$stage \
  --export-type=swagger \
  --accept=application/json \
  --region=$region \
  $outputFileName

node util/extract-option.js

npx redoc-cli bundle -o index.html ./documentation.json
